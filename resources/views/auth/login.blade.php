
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/bracket/img/bracket-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracket">
    <meta property="og:title" content="Bracket">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracket/img/bracket-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracket/img/bracket-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>Bracket Responsive Bootstrap 4 Admin Template</title>

    <!-- vendor css -->
    <link href="{{ asset('assets/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/ionicons.css') }}" rel="stylesheet">
 <link href="{{ asset('assets/css/perfect-scrollbar.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/jquery.switchButton.css') }}" rel="stylesheet">
    <!-- Bracket CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/bracket.css') }}">
  </head>

  <body >

    <div class="d-flex align-items-center justify-content-center ht-100v" style="background:url('assets/images/background.png');">

    </div><!-- d-flex -->


<!-- MODAL GRID -->
              <div id="modaldemo6" class="modal fade">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content bd-0 bg-transparent rounded overflow-hidden">
                    <div class="modal-body pd-0">
                      <div class="row no-gutters">
                        <div class="col-lg-6 bg-danger">
                          <div class="pd-40">
                            <h4 class="tx-white mg-b-20"><span>[</span> New Cargo <span>]</span></h4>
                            <p class="tx-white op-7 mg-b-60">We released new cargo app and dashboard with more features.
                            Now, you can use with new features.</p>
                            <p class="tx-white tx-13">
                              <span class="tx-uppercase tx-medium d-block mg-b-15">Included Features:</span>
                              <span class="op-7">
                                <ul>
                                    <li>Manage Inbound Waybills</li>
                                    <li>Manage Outbound Waybills</li>
                                    <li>View Status For Inbounds & Outbounds</li>
                                    <li>Reports For Inbounds & Outbounds</li>
                                    <li>Download Inbounds & Outbouds Excel Files</li>
                                    <li>Saved Logs For Inbounds & Outbounds</li>
                                    <li>Support Scanned Waybills For App & Web</li>
                                    <li>Search Waybills With Full Logs</li>
                                    <li>Voice Message Sent & More ...</li>
                                </ul>
                                </span>
                            </p>
                            </div>
                          </div><!-- col-6 -->
                          <div class="col-lg-6 bg-white">
                            <div class="pd-30">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                              <div class="pd-x-30 pd-y-10">
                                <h3 class="tx-inverse  mg-b-5">Welcome back!</h3>
                                <p>Sign in to your account to continue</p>
                                <br>
                                <div class="form-group">
                                  <input type="email" name="email" class="form-control pd-y-12" placeholder="user@royalx.net">
                                </div><!-- form-group -->
                                <div class="form-group mg-b-20">
                                  <input type="email" name="password" class="form-control pd-y-12" placeholder="********">
                                  <span class="tx-12 d-block mg-t-10">Forgot password?</span>
                                </div><!-- form-group -->

                                <button class="btn btn-danger pd-y-12 btn-block">Sign In</button>

                                <div class="mg-t-30 mg-b-20 tx-12">Don't have an account yet? <span>Contat IT Dept</span></div>
                              </div>
                            </div><!-- pd-20 -->
                          </div><!-- col-6 -->
                        </div><!-- row -->

                      </div><!-- modal-body -->
                    </div><!-- modal-content -->
                  </div><!-- modal-dialog -->
                </div><!-- modal -->
    <script src="{{ asset('assets/js/jquery.js') }}"></script>
    <script src="{{ asset('assets/js/popper.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.js') }}"></script>
    <script src="{{ asset('assets/js/perfect-scrollbar.jquery.js') }}"></script>
    <script src="{{ asset('assets/js/moment.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-ui.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.switchButton.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.peity.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.js') }}"></script>
<script>
      $(function(){
        $("#modaldemo6").modal('show');
        // showing modal with effect
        $('.modal-effect').on('click', function(){
          var effect = $(this).attr('data-effect');
          $('#modaldemo8').addClass(effect, function(){
            $('#modaldemo8').modal('show');
          });
          return false;
        });

        // hide modal with effect
        $('#modaldemo8').on('hidden.bs.modal', function (e) {
          $(this).removeClass (function (index, className) {
              return (className.match (/(^|\s)effect-\S+/g) || []).join(' ');
          });
        });
      });
    </script>
  </body>
</html>
